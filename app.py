#!/usr/bin/env python

from gevent.pywsgi import WSGIServer
from gevent import monkey, sleep

# monkeypatch all socket things before importing anything else
monkey.patch_all()

import os
from urllib import parse as urlparse


class App(object):
    def __init__(self, *args, **kwargs):
        # Read homepage and hold in memory.
        here = os.path.dirname(os.path.abspath(__file__))
        with open(os.path.join(here, 'home.html'), 'r') as f:
            self.home_html = f.read()

        with open(os.path.join(here, 'multi.html'), 'r') as f:
            self.multi_html = f.read()

    def __call__(self, environ, start_response):
        path = urlparse.urlparse(environ['PATH_INFO']).path
        if path in ('', '/'):
            return self.homepage(environ, start_response)
        elif path == '/events/':
            return self.events(environ, start_response)
        elif path == '/multi/':
            return self.multi(environ, start_response)

        return self.four_o_four(environ, start_response)

    def homepage(self, environ, start_response):
        start_response("200 OK", [("Content-Type", "text/html")])
        return [self.home_html.encode('utf-8')]

    def multi(self, environ, start_response):
        start_response("200 OK", [("Content-Type", "text/html")])
        return [self.multi_html.encode('utf-8')]


    def events(self, environ, start_response):
        start_response("200 OK", [("Content-Type", "text/event-stream")])

        counter = 0
        while True:
            counter += 1
            # The simplest SSE event is a single "data: " line, terminated with
            # two newlines.
            yield ("data: %s\n\n" % counter).encode('utf-8')
            sleep(1)

    def four_o_four(self, environ, start_response):
        start_response("404 NOT FOUND", [("Content-Type", "text/plain")])
        return [b"404 Not Found"]


if __name__ == '__main__':
    address = '0.0.0.0', int(os.getenv('PORT', 8000))
    server = WSGIServer(address, App())
    try:
        print("Server running on port %s:%d. Ctrl+C to quit" % address)
        server.serve_forever()
    except KeyboardInterrupt:
        server.stop()
        print("Bye bye")
