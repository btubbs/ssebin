======
SSEBin
======

This is a tiny application with two tiny purposes:

1. Allow people to check their browser's compatibility with Server Sent Events.
2. Show a little of their awesomeness.

It uses Gevent.  The app is configured for running on Heroku and/or Velociraptor.  
You can see a copy running at ssebin.btubbs.com_.

.. _ssebin.btubbs.com: http://ssebin.btubbs.com/
